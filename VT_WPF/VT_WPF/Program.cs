﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VT_WPF_DLL;
using System.Data.OleDb;

namespace VT_WPF
{
    class Program
    {

        static void Main(string[] args)
        {
            IValue value = new FixedValues();
            Vocabulary v;
            try
            {
                switch (Convert.ToInt32(args[0]))
                {
                    case 0:
                        value = new FixedValues();
                        break;

                    case 1:
                        value = new ValuesfromTXT();
                        break;

                    case 2:
                        value = new ValuesfromDataTable();
                        break;
                }      
            }
            catch (Exception)
            {
                throw new Exception("Falscher Eingabewert in der Kommandozeile");
            }
            finally
            {
                v = new Vocabulary(value);
                dbCheck(v);
                //ins2Result();
                //Result();
            }
        }

        static void dbCheck(Vocabulary v)
        {
            foreach (Word w in v.wList)
            {
                Console.WriteLine(w.EWord + " " + w.TranslatedWord);
            }
        }


        static void ins2Result()
        {
            //string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb";
            //OleDbConnection con = new OleDbConnection(cs);
            //con.Open();

            OleDbConnection con = VTConnection.Instance();


            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "insert into results (positive, negative, exdate) values (4, 6, '" + DateTime.Now + "')" ;
            int i = cmd.ExecuteNonQuery();

            //con.Close();
            con = VTConnection.CloseConnection();
        }

        static void Result()
        {
            //string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb";
            //OleDbConnection con = new OleDbConnection(cs);

            OleDbConnection con = VTConnection.Instance();

            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd1 = new OleDbCommand();
            cmd.Connection = con;
            cmd1.Connection = con;
            cmd.CommandText = "select positive, negative, percents, id from results";
          

            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                if(reader.GetValue(2) == System.DBNull.Value)
                {
                    double percent;
                    int positive = reader.GetInt32(0);
                    int negative = reader.GetInt32(1);
                    int id = reader.GetInt32(3);

                
                    percent = ((double)positive / (positive + negative))*100;
                   

                    cmd1.CommandText = "update results set percents='" + percent + "' where id=" + id+"";
                    cmd1.ExecuteNonQuery();
                }
                
            }
            reader.Close();

            con = VTConnection.CloseConnection();
        }
    }
}
