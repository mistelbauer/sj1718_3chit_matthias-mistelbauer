﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace VT_WPF
{
    class VTConnection
    {

        private static OleDbConnection con = null;

        private VTConnection()
        {
            con.Open();
        }


        public static OleDbConnection Instance()
        {
            try
            {
                if (con == null)
                    con = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb");

                con.Open();
                return con;
            }
            catch (Exception e)
            {                
                return null;
            }
            
        }

        public static OleDbConnection CloseConnection()
        {
            if (con != null)
            {
                con.Close();
                con = null;
            }
                
            
            return con;
        }


    }
}
