﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VT_WPF_DLL
{
    public class FixedValues : IValue
    {
 

        public List<Word> CreateList()
        {
            List<Word> wList = new List<Word>();


            wList.Add(new Word { EWord = "cat", TranslatedWord = "Katze" });
            wList.Add(new Word { EWord = "frog", TranslatedWord = "Frosch" });
            wList.Add(new Word { EWord = "shark", TranslatedWord = "Hai" });
            wList.Add(new Word { EWord = "crab", TranslatedWord = "Krebs" });
            wList.Add(new Word { EWord = "jellyfish", TranslatedWord = "Qualle" });
            wList.Add(new Word { EWord = "fly", TranslatedWord = "Fliege" });
            wList.Add(new Word { EWord = "lizard", TranslatedWord = "Eidechse" });
            wList.Add(new Word { EWord = "snake", TranslatedWord = "Schlange" });
            wList.Add(new Word { EWord = "bear", TranslatedWord = "Bär" });
            wList.Add(new Word { EWord = "dolphin", TranslatedWord = "Delfin" });
            wList.Add(new Word { EWord = "elephant", TranslatedWord = "Elefant" });
            wList.Add(new Word { EWord = "elk", TranslatedWord = "Elch" });
            wList.Add(new Word { EWord = "fox", TranslatedWord = "Fuchs" });
            wList.Add(new Word { EWord = "giraffe", TranslatedWord = "Giraffe" });
            wList.Add(new Word { EWord = "hamster", TranslatedWord = "Hamster" });
            wList.Add(new Word { EWord = "dog", TranslatedWord = "Hund" });
            wList.Add(new Word { EWord = "hedgehog", TranslatedWord = "Igel" });
            wList.Add(new Word { EWord = "mouse", TranslatedWord = "Maus" });
            wList.Add(new Word { EWord = "mole", TranslatedWord = "Maulwurf" });
            wList.Add(new Word { EWord = "eagle", TranslatedWord = "Adler" });

            return wList;
        }

     
    }
}
