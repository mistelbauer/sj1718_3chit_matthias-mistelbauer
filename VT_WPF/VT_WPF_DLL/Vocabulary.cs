﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.OleDb;

namespace VT_WPF_DLL
{
    public class Vocabulary
    {

        public List<Word> wList = new List<Word>();

        //public List<Word> wordList
        //{
        //    get
        //    {
        //        SortRandomly()
        //        return wList;
        //    }
        //}

        //int pointer;


        public Vocabulary(IValue value)
        {
            wList = value.CreateList();

            SortRandomly();

        }

        public void SortRandomly()
        {
            Random rng = new Random();

            int n = wList.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                Word value = wList[k];
                wList[k] = wList[n];
                wList[n] = value;
            }
        }

        public bool Check(string word, string translation)
        {
            if (word == translation)
                return true;
            else
                return false;
        }

    
    }
}