﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace VT_WPF_DLL
{
    public class ValuesfromDataTable : IValue
    {

        public List<Word> wList;

        public ValuesfromDataTable()
        {
            wList = new List<Word>();
            CreateList();
        }

        public List<Word> CreateList()
        {
            string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb";
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            //OleDbConnection con = VTConnection.Instance();

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select english, german from words";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                wList.Add(new Word { EWord = reader.GetString(0), TranslatedWord = reader.GetString(1) });
            }

            reader.Close();

            con.Close();

            return wList;
        }
    }
}
