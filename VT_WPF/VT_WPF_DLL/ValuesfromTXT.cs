﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VT_WPF_DLL
{
    public class ValuesfromTXT : IValue
    {

        public List<Word> wList;

        StreamReader sr;

        public ValuesfromTXT()
        {
            wList = new List<Word>();
            CreateList();
        }

        public List<Word> CreateList()
        {
            if (File.Exists("words.txt"))
            {
                //try
                //{

                sr = new StreamReader("words.txt");

                while (sr.Peek() != -1)
                {
                    string zeile = sr.ReadLine();
                    string[] info = zeile.Split(';');

                    wList.Add(new Word { EWord = info[0], TranslatedWord = info[1] });
                }

                sr.Close();

                return wList;



                //}
                //catch
                //{
                //    throw new FileNotFoundException();
                //}
            }
            return wList;
        }
    }
}   
