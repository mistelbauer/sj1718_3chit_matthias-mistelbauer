﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ÜbungEventhandling_2_Lift
{
    class Person
    {
        public string Name { get; set; }

        public double Weight { get; set; }

        public override string ToString()
        {
            return Name + ": " + Weight;
        }


    }
}
