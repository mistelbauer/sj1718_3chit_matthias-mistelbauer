﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ÜbungEventhandling_2_Lift
{
    class Lift
    {
        public string LiftID { get; set; }

        public double MaxWeightinKG { get; set; }

        private List<Person> liste;

        public delegate void OverloadLift(Lift hänkel, Person name, List<Person> person);

        public event OverloadLift overload;

        public double gesamt = 0;

        public void zusteigen(Person p)
        {
            if (gesamt + p.Weight < this.MaxWeightinKG)
            {
                liste.Add(p);
                gesamt += p.Weight;
            }
            else
            {
                liste.Add(p);
                overload(this, p, liste);
            }
                
                
        }

        public override string ToString()
        {
            String ausgabe = "Personen im Lift: ";
            foreach (Person p in liste)
                ausgabe += "\n" + p;
            return ausgabe;
        }

        public Lift()
        {
            liste = new List<Person>();
        }

    }
}
