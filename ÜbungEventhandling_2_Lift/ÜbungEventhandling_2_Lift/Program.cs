﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ÜbungEventhandling_2_Lift
{
    class Program
    {
        static void Main(string[] args)
        {
            Lift l1 = new Lift { LiftID = "2344/22", MaxWeightinKG = 1250 };
            l1.overload += LeiderNicht;
            //Person p1 = new Person { Name = "Max", Weight = 65 };
            //Person p2 = new Person { Name = "Lara", Weight = 100 };
            //Person p3 = new Person { Name = "Franz", Weight = 62 };

            Random zahl = new Random();
            Person p = new Person();
            int index = 1;

            while (l1.gesamt + p.Weight < l1.MaxWeightinKG)
            {
                
                string personenname = "Person" + index;

                p = new Person { Name = personenname, Weight = zahl.Next(30, 200) };
                l1.zusteigen(p);
                index++;
            }


            Console.WriteLine(l1);


        }

        public static void LeiderNicht(Lift l, Person p, List<Person> liste)
        {
            double übergewicht = l.gesamt - l.MaxWeightinKG;

            foreach (Person pe in liste)
            {
                if (pe.Weight > übergewicht)
                {
                    Console.WriteLine("Lift: " + l.LiftID + " zu schwer! Person: " + pe.Name + " sollte den Lift verlassen!");
                    break;
                }
            }

            Console.WriteLine("Lift: " + l.LiftID + " zu schwer! Person: " + p.Name + " muss den Lift verlassen!");
        }
    }
}
