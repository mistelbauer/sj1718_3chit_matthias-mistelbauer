﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace VTForm
{
    class Options
    {
        StreamReader sr;

        public Options()
        {
            try
            {
                ReadFile();
            }
            catch (VTException ex)
            {
                MessageBox.Show(ex.Message);
            }
            

        }

        private bool isDirty = false;
        private string direction;
        public string Direction
        {
            get { return this.direction; }
            set
            {
                if (this.direction != value)
                {
                    this.direction = value;
                    isDirty = true;
                }
            }
        }

        private int maxWords;
        public int MaxWords
        {
            get { return this.maxWords; }
            set
            {
                if (this.maxWords != value)
                {
                    this.maxWords = value;
                    isDirty = true;
                }
            }
        }

        private List<Grade> gradeList;
        public List<Grade> GradeList
        {
            get { return this.gradeList; }
            set
            {
                if (this.gradeList != value)
                {
                    this.gradeList = value;
                    isDirty = true;
                }
            }
        }


        public void writeToFile()
        {
            try
            {
                StreamWriter sw = new StreamWriter("einstellungen.txt");

                if (isDirty)
                {
                    sw.WriteLine(Direction);
                    sw.WriteLine(MaxWords);
                    foreach (Grade i in GradeList)
                    {
                        sw.WriteLine(i.percent + ";" + i.grade + ";" + i.value);
                    }
                    //sw.WriteLine(GradeList);
                    isDirty = false;
                }
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {
                VTException ex = new VTException();

                throw new VTException(Convert.ToString(DateTime.Now) + "//: Datei ist schreibgeschützt!", ex);
            }
        }


        public void ReadFile()
        {
            if (File.Exists("einstellungen.txt"))
            {
                try
                {
                    //File.Delete("einstellungen.txt");

                    //System.Threading.Thread(5000);
                    sr = new StreamReader("einstellungen.txt");


                    Direction = sr.ReadLine();
                    MaxWords = Convert.ToInt32(sr.ReadLine());

                    List<Grade> gl = new List<Grade>();


                    while (sr.Peek() != -1)
                    {
                        string zeile = sr.ReadLine();
                        string[] info = zeile.Split(';');


                        gl.Add(new Grade(Convert.ToInt32(info[0]), info[1], Convert.ToInt32(info[2])));
                    }

                    GradeList = gl;

                    sr.Close();
                }
                catch
                {
                    VTException ex = new VTException();

                    throw new VTException(Convert.ToString(DateTime.Now) + "//: Datei existiert nicht mehr!", ex);
                }

            }
        }


        
    }
}





