﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VTForm
{
    class VTException:ApplicationException
    {
        public VTException() { }
        public VTException(string message) : base(message) { }
        public VTException(string message, Exception inner) 
         : base(message, inner) {

            WriteToFile(message, inner);

        }

        public void WriteToFile(string message, Exception inner)
        {
            StreamWriter sw = new StreamWriter("vt_errors.log", true);

            sw.Write(message + " // ");
            sw.WriteLine(Convert.ToString(inner));

            sw.Flush();
            sw.Close();
        }


    }
}
