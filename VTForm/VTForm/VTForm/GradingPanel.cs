﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace VTForm
{
    class GradingPanel:Panel
    {
        public NumericUpDown percent;
        public TextBox grade;
        public TextBox value;

        public GradingPanel()
        {
            

            percent = new NumericUpDown();
            grade = new TextBox();
            value = new TextBox();

            int left = 25;

            percent.Width = 100;
            percent.Left = left;

            left += 105;

            grade.Width = 100;
            grade.Left = left;

            left += 105;

            value.Width = 100;
            value.Left = left;

            this.Controls.Add(percent);
            this.Controls.Add(grade);
            this.Controls.Add(value);
        }
        
       


      
    }
}
