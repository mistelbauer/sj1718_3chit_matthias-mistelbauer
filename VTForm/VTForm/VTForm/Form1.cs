﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VTrainer;

namespace VTForm
{
    public partial class Form1 : Form
    {
        Options options;
        int countGreen;
        int anzahl;
        string ergebnis;

        public Form1()
        {
            InitializeComponent();
            options = new Options();
            

        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void newTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel.Controls.Clear();
            int topHeight = 25;
            int leftWidth = 35;
           
            List<Vokabel> vocabs = new List<Vokabel>();
            Vokabeltrainer vocabt = new Vokabeltrainer();
            vocabs = vocabt.GetList();

            anzahl = 0;

            if(options.Direction == "EN-DE")
                Englisch_Deutsch(ref anzahl, topHeight, leftWidth, vocabs);
            else
                Deutsch_Englisch(ref anzahl, topHeight, leftWidth, vocabs);

            
            
            anz.Text = "Dictionary Entries: " + anzahl;

        }

        private void Englisch_Deutsch(ref int anzahl, int topHeight, int leftWidth, List<Vokabel> vocabs)
        {
            int index = 0;

            foreach (Vokabel i in vocabs)
            {
                if (index == options.MaxWords)
                    break;
                Label label = new Label();
                label.Text = i.English;
                label.Height = 25;
                label.Width = 100;
                //label.Font.Size
                label.Top = topHeight;
                label.Left = leftWidth;
                panel.Controls.Add(label);

                TextBox tb = new TextBox();
                tb.Height = 25;
                tb.Width = 100;
                tb.Top = topHeight;
                tb.Left = 200;
                tb.Tag = i.English;
                panel.Controls.Add(tb);

                topHeight += 27;
                index++;
                anzahl++;

            }
        }

        private void Deutsch_Englisch(ref int anzahl, int topHeight, int leftWidth, List<Vokabel> vocabs)
        {
            int index = 0;

            foreach (Vokabel i in vocabs)
            {
                if (index == options.MaxWords)
                    break;
                Label label = new Label();
                label.Text = i.German;
                label.Height = 25;
                label.Width = 100;
                //label.Font.Size
                label.Top = topHeight;
                label.Left = leftWidth;
                panel.Controls.Add(label);

                TextBox tb = new TextBox();
                tb.Height = 25;
                tb.Width = 100;
                tb.Top = topHeight;
                tb.Left = 200;
                tb.Tag = i.German;
                panel.Controls.Add(tb);

                topHeight += 27;
                index++;
                anzahl++;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            List<Vokabel> vocabs = new List<Vokabel>();
            Vokabeltrainer vocabt = new Vokabeltrainer();
            vocabs = vocabt.GetList();

            
            
            foreach(Control c in panel.Controls)
            {
                if(options.Direction == "EN-DE")
                {
                    if (c is TextBox)
                    {
                        if (vocabt.Check(c.Text, (string)c.Tag))
                        {
                            c.BackColor = Color.Green;
                            countGreen++;
                        }
                            
                        
                        else
                            c.BackColor = Color.Red;

                    }
                }
                else
                {
                    if (c is TextBox)
                    {
                        if (vocabt.Check((string)c.Tag, c.Text))
                        {
                            c.BackColor = Color.Green;
                            countGreen++;
                        }
                        else
                            c.BackColor = Color.Red;

                    }
                }
            }

            int calPercent = (countGreen * 100) / anzahl;
            
            foreach (Grade g in options.GradeList)
            {
                if (calPercent >= g.percent)
                {
                    ergebnis = g.grade;
                    break;
                }                    

            }

            MessageBox.Show(Convert.ToString(ergebnis));


        }

        private void settings_Click(object sender, EventArgs e)
        {
            Einstellungen_Form edialog = new Einstellungen_Form();
            DialogResult r = edialog.ShowDialog(); // modal

            List<Grade> gl = new List<Grade>();

            if (r == DialogResult.OK)
            {
                foreach (Control c in edialog.panelGrade.Controls)
                {
                    if (c is GradingPanel)
                    {
                        GradingPanel h = (GradingPanel)c;

                        gl.Add(new Grade(Convert.ToInt32(h.percent.Value), Convert.ToString(h.grade.Text), Convert.ToInt32(h.value.Text)));


                    }
                }

                options.Direction = (string)edialog.direction_box.SelectedItem;
                options.MaxWords = Convert.ToInt32(edialog.maxwords_box.Value);
                options.GradeList = gl;

                try
                {
                    options.writeToFile();
                    MessageBox.Show("Changes were saved!");
                }
                catch (VTException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
                
            }

            //edialog.Show(); // nicht modal
        }

        private void info_Click(object sender, EventArgs e)
        {
            Information info = new Information();
            info.ShowDialog();
        }

        

        

        private void addRow_Click(object sender, EventArgs e)
        {


        }
    }
}
