﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTForm
{
    class Grade
    {

        public int percent;
        public string grade;
        public int value;


        public Grade(int percent, string grade, int value)
        {
            this.percent = percent;
            this.grade = grade;
            this.value = value;
        }

    }
}
