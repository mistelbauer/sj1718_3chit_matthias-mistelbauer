﻿namespace FormsDynButton
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.zeile = new System.Windows.Forms.NumericUpDown();
            this.spalte = new System.Windows.Forms.NumericUpDown();
            this.button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.zeile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spalte)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Zeile";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(312, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Spalte";
            // 
            // zeile
            // 
            this.zeile.Location = new System.Drawing.Point(175, 48);
            this.zeile.Margin = new System.Windows.Forms.Padding(4);
            this.zeile.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.zeile.Name = "zeile";
            this.zeile.Size = new System.Drawing.Size(73, 22);
            this.zeile.TabIndex = 2;
            // 
            // spalte
            // 
            this.spalte.Location = new System.Drawing.Point(409, 48);
            this.spalte.Margin = new System.Windows.Forms.Padding(4);
            this.spalte.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spalte.Name = "spalte";
            this.spalte.Size = new System.Drawing.Size(73, 22);
            this.spalte.TabIndex = 3;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(569, 41);
            this.button.Margin = new System.Windows.Forms.Padding(4);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(229, 36);
            this.button.TabIndex = 4;
            this.button.Text = "Buttons anzeigen!";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 689);
            this.Controls.Add(this.button);
            this.Controls.Add(this.spalte);
            this.Controls.Add(this.zeile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.zeile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spalte)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown zeile;
        private System.Windows.Forms.NumericUpDown spalte;
        private System.Windows.Forms.Button button;
    }
}

