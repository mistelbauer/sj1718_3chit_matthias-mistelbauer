﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsDynButton
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            int y = Convert.ToInt32(zeile.Text);
            int x = Convert.ToInt32(spalte.Text);

            int top_height = 110;
            int left_width = 70;

            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    SuperButton b = new SuperButton();
                    b.Top = top_height;
                    b.Left = left_width;
                    b.Height = 30;                   
                    b.Width = 45;
                    b.Text = Convert.ToString(j) + " / " + Convert.ToString(i);
                    b.Click += new System.EventHandler(click);
                    this.Controls.Add(b);

                    left_width += 47;

                }
                top_height += 32;
                left_width = 70;
            }

        }

        public void click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            MessageBox.Show(b.Text + " wurde gedrückt!");
        }

       
    }
}
