﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exception_tester.doer_pattern
{
    class MyFirstExeption:ApplicationException
    {
        public MyFirstExeption() { }
        public MyFirstExeption(string message) : base(message) { }
        public MyFirstExeption(string message, ApplicationException inner) : base(message, inner) { }
    }
}
