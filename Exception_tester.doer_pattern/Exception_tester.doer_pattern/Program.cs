﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exception_tester.doer_pattern
{
    class Program
    {
        static void Main(string[] args)
        {

            List<int> list = new List<int>();

            double avg = Average(list);

            double av;
            bool avgbool = AverageTryParse(list, out av);



            Console.WriteLine(avg);

            //if (avgbool == false)
            //    Console.WriteLine("Geht ned");
            //else
                Console.WriteLine(av);


        }

        static double Average(List<int> list)
        {
            try
            {
                double avg;
                int sum = 0;

                foreach (int i in list)
                {
                    sum += i;
                }

                avg = sum / list.Count();

                return avg;
            }
            catch
            {
                //Console.WriteLine("Geht ned! Keine Divisionen durch 0 erlaubt!");
                ApplicationException ae = new ApplicationException();
                throw new MyFirstExeption("Geht ned! Keine Divisionen durch 0 erlaubt!",ae);
                //return 0;
            }

        }

        static public bool AverageTryParse(List<int> l, out double av)
        {
            try
            {
                int sum = 0;

                foreach (int i in l)
                {
                    sum += i;
                }

                av = sum / l.Count();

                return true;
            }
            catch
            {
                av = 0;
                return false;
            }

        }

    }


}
