﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            string geschlecht;

            if (radioButton_männlich.Checked)
                geschlecht = "männlich";
            else
                geschlecht = "weiblich";


            string check1;

            if (checkBox1.Checked)
                check1 = "Präsenzdienst wurde geleistet!";
            else
                check1 = "Präsenzdienst wurde nicht geleistet!";

            string check2;

            if (checkBox2.Checked)
                check2 = "Teilnahmebedingungen akzeptiert!";
            else
                check2 = "Teilnahmebedingungen nicht akzeptiert!";


            MessageBox.Show("Sie haben isch mit folgenden Daten bei uns angemeldet:" + "\n" + "\n" +
                VornameBox.Text + " " + NachnameBox.Text + "\n" +
                AnschriftBox.Text + "\n" +
                Geburtsjahr.Text + "\t" + geschlecht + "\n" +
                check1 + "\n" +
                check2);
        }
    }
}
