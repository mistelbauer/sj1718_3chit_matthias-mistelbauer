﻿namespace WindowsForms1
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label_Vorname = new System.Windows.Forms.Label();
            this.VornameBox = new System.Windows.Forms.TextBox();
            this.NachnameBox = new System.Windows.Forms.TextBox();
            this.label_Nachname = new System.Windows.Forms.Label();
            this.AnschriftBox = new System.Windows.Forms.TextBox();
            this.label_Anschrift = new System.Windows.Forms.Label();
            this.plzBox = new System.Windows.Forms.TextBox();
            this.label_PLZ = new System.Windows.Forms.Label();
            this.OrtBox = new System.Windows.Forms.TextBox();
            this.label_Ort = new System.Windows.Forms.Label();
            this.label_Geburtsjahr = new System.Windows.Forms.Label();
            this.Geburtsjahr = new System.Windows.Forms.DomainUpDown();
            this.radioButton_weiblich = new System.Windows.Forms.RadioButton();
            this.radioButton_männlich = new System.Windows.Forms.RadioButton();
            this.label_Geschlecht = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(235, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(679, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Teilnahme zum Projekt \"Regionale Innovationen\"";
            // 
            // label_Vorname
            // 
            this.label_Vorname.AutoSize = true;
            this.label_Vorname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Vorname.Location = new System.Drawing.Point(111, 146);
            this.label_Vorname.Name = "label_Vorname";
            this.label_Vorname.Size = new System.Drawing.Size(93, 24);
            this.label_Vorname.TabIndex = 1;
            this.label_Vorname.Text = "Vorname:";
            // 
            // VornameBox
            // 
            this.VornameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VornameBox.Location = new System.Drawing.Point(310, 146);
            this.VornameBox.Name = "VornameBox";
            this.VornameBox.Size = new System.Drawing.Size(221, 28);
            this.VornameBox.TabIndex = 2;
            // 
            // NachnameBox
            // 
            this.NachnameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NachnameBox.Location = new System.Drawing.Point(310, 199);
            this.NachnameBox.Name = "NachnameBox";
            this.NachnameBox.Size = new System.Drawing.Size(221, 28);
            this.NachnameBox.TabIndex = 4;
            // 
            // label_Nachname
            // 
            this.label_Nachname.AutoSize = true;
            this.label_Nachname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Nachname.Location = new System.Drawing.Point(111, 199);
            this.label_Nachname.Name = "label_Nachname";
            this.label_Nachname.Size = new System.Drawing.Size(108, 24);
            this.label_Nachname.TabIndex = 3;
            this.label_Nachname.Text = "Nachname:";
            // 
            // AnschriftBox
            // 
            this.AnschriftBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AnschriftBox.Location = new System.Drawing.Point(310, 251);
            this.AnschriftBox.Name = "AnschriftBox";
            this.AnschriftBox.Size = new System.Drawing.Size(221, 28);
            this.AnschriftBox.TabIndex = 6;
            // 
            // label_Anschrift
            // 
            this.label_Anschrift.AutoSize = true;
            this.label_Anschrift.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Anschrift.Location = new System.Drawing.Point(111, 251);
            this.label_Anschrift.Name = "label_Anschrift";
            this.label_Anschrift.Size = new System.Drawing.Size(87, 24);
            this.label_Anschrift.TabIndex = 5;
            this.label_Anschrift.Text = "Anschrift:";
            // 
            // plzBox
            // 
            this.plzBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plzBox.Location = new System.Drawing.Point(192, 300);
            this.plzBox.Name = "plzBox";
            this.plzBox.Size = new System.Drawing.Size(107, 28);
            this.plzBox.TabIndex = 8;
            // 
            // label_PLZ
            // 
            this.label_PLZ.AutoSize = true;
            this.label_PLZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PLZ.Location = new System.Drawing.Point(111, 303);
            this.label_PLZ.Name = "label_PLZ";
            this.label_PLZ.Size = new System.Drawing.Size(49, 24);
            this.label_PLZ.TabIndex = 7;
            this.label_PLZ.Text = "PLZ:";
            // 
            // OrtBox
            // 
            this.OrtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrtBox.Location = new System.Drawing.Point(379, 300);
            this.OrtBox.Name = "OrtBox";
            this.OrtBox.Size = new System.Drawing.Size(242, 28);
            this.OrtBox.TabIndex = 10;
            // 
            // label_Ort
            // 
            this.label_Ort.AutoSize = true;
            this.label_Ort.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Ort.Location = new System.Drawing.Point(324, 303);
            this.label_Ort.Name = "label_Ort";
            this.label_Ort.Size = new System.Drawing.Size(40, 24);
            this.label_Ort.TabIndex = 9;
            this.label_Ort.Text = "Ort:";
            // 
            // label_Geburtsjahr
            // 
            this.label_Geburtsjahr.AutoSize = true;
            this.label_Geburtsjahr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Geburtsjahr.Location = new System.Drawing.Point(111, 388);
            this.label_Geburtsjahr.Name = "label_Geburtsjahr";
            this.label_Geburtsjahr.Size = new System.Drawing.Size(118, 24);
            this.label_Geburtsjahr.TabIndex = 11;
            this.label_Geburtsjahr.Text = "Gerburtsjahr:";
            // 
            // Geburtsjahr
            // 
            this.Geburtsjahr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Geburtsjahr.Items.Add("1980");
            this.Geburtsjahr.Items.Add("1981");
            this.Geburtsjahr.Items.Add("1982");
            this.Geburtsjahr.Items.Add("1983");
            this.Geburtsjahr.Location = new System.Drawing.Point(310, 388);
            this.Geburtsjahr.Name = "Geburtsjahr";
            this.Geburtsjahr.Size = new System.Drawing.Size(140, 28);
            this.Geburtsjahr.TabIndex = 12;
            // 
            // radioButton_weiblich
            // 
            this.radioButton_weiblich.AutoSize = true;
            this.radioButton_weiblich.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_weiblich.Location = new System.Drawing.Point(747, 387);
            this.radioButton_weiblich.Name = "radioButton_weiblich";
            this.radioButton_weiblich.Size = new System.Drawing.Size(100, 28);
            this.radioButton_weiblich.TabIndex = 13;
            this.radioButton_weiblich.TabStop = true;
            this.radioButton_weiblich.Text = "weiblich";
            this.radioButton_weiblich.UseVisualStyleBackColor = true;
            // 
            // radioButton_männlich
            // 
            this.radioButton_männlich.AutoSize = true;
            this.radioButton_männlich.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_männlich.Location = new System.Drawing.Point(747, 421);
            this.radioButton_männlich.Name = "radioButton_männlich";
            this.radioButton_männlich.Size = new System.Drawing.Size(108, 28);
            this.radioButton_männlich.TabIndex = 14;
            this.radioButton_männlich.TabStop = true;
            this.radioButton_männlich.Text = "männlich";
            this.radioButton_männlich.UseVisualStyleBackColor = true;
            // 
            // label_Geschlecht
            // 
            this.label_Geschlecht.AutoSize = true;
            this.label_Geschlecht.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Geschlecht.Location = new System.Drawing.Point(585, 388);
            this.label_Geschlecht.Name = "label_Geschlecht";
            this.label_Geschlecht.Size = new System.Drawing.Size(110, 24);
            this.label_Geschlecht.TabIndex = 15;
            this.label_Geschlecht.Text = "Geschlecht:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(113, 462);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(224, 28);
            this.checkBox1.TabIndex = 16;
            this.checkBox1.Text = "Präsenzdienst geleistet";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(113, 496);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(388, 28);
            this.checkBox2.TabIndex = 17;
            this.checkBox2.Text = "Ich akzeptiere die Teilnahmebedingungen";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button
            // 
            this.button.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button.Location = new System.Drawing.Point(353, 549);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(471, 84);
            this.button.TabIndex = 18;
            this.button.Text = "Senden";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 664);
            this.Controls.Add(this.button);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label_Geschlecht);
            this.Controls.Add(this.radioButton_männlich);
            this.Controls.Add(this.radioButton_weiblich);
            this.Controls.Add(this.Geburtsjahr);
            this.Controls.Add(this.label_Geburtsjahr);
            this.Controls.Add(this.OrtBox);
            this.Controls.Add(this.label_Ort);
            this.Controls.Add(this.plzBox);
            this.Controls.Add(this.label_PLZ);
            this.Controls.Add(this.AnschriftBox);
            this.Controls.Add(this.label_Anschrift);
            this.Controls.Add(this.NachnameBox);
            this.Controls.Add(this.label_Nachname);
            this.Controls.Add(this.VornameBox);
            this.Controls.Add(this.label_Vorname);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_Vorname;
        private System.Windows.Forms.TextBox VornameBox;
        private System.Windows.Forms.TextBox NachnameBox;
        private System.Windows.Forms.Label label_Nachname;
        private System.Windows.Forms.TextBox AnschriftBox;
        private System.Windows.Forms.Label label_Anschrift;
        private System.Windows.Forms.TextBox plzBox;
        private System.Windows.Forms.Label label_PLZ;
        private System.Windows.Forms.TextBox OrtBox;
        private System.Windows.Forms.Label label_Ort;
        private System.Windows.Forms.Label label_Geburtsjahr;
        private System.Windows.Forms.DomainUpDown Geburtsjahr;
        private System.Windows.Forms.RadioButton radioButton_weiblich;
        private System.Windows.Forms.RadioButton radioButton_männlich;
        private System.Windows.Forms.Label label_Geschlecht;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button button;
    }
}

